传真

### 3.1.1基本信息表



传真发送表 fax_send

传真发送单位关系表 fax_send_unit

传真发送文件关联表 fax_send_file

接收传真表 fax_receiver

传真单位表 fax_unit

传真黑名单 fax_blacklist

### 3.1.2.组件依赖



###  3.1.3.功能介绍

管理传真收发。

### 3.1.4.接口介绍

查看swagger接口文档。

### 3.1.5.nacos配置

richway-fax-dev.yml

~~~yaml
#配置
spring:
  redis:
    host: 127.0.0.1
    port: 6379
    password: 
# json时间格式设置
  jackson:
    time-zone: GMT+8
    date-format: yyyy-MM-dd HH:mm:ss
  # 设置上传文件大小
  servlet:
    multipart.max-file-size: 100MB
    multipart.max-request-size: 100MB
  # 连接池配置
  datasource:
    url: jdbc:mysql://localhost:3306/richway-cloud-sys?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=GMT%2B8
    username: root
    password: mima123
    driver-class-name: com.mysql.cj.jdbc.Driver
    type: com.alibaba.druid.pool.DruidDataSource
    druid:
      initial-size: 5
      min-idle: 5
      max-active: 20
      max-wait: 30000
      time-between-eviction-runs-millis: 60000
      min-evictable-idle-time-millis: 300000
      test-while-idle: true
      test-on-borrow: true
      test-on-return: false
      remove-abandoned: true
      remove-abandoned-timeout: 1800
      #pool-prepared-statements: false
      #max-pool-prepared-statement-per-connection-size: 20
      filters: stat,wall
      validation-query: SELECT 'x'
      stat-view-servlet:
        url-pattern: /druid/*
        reset-enable: true
        login-username: admin
        login-password: admin

# Mybatis-plus配置
mybatis-plus:
  mapper-locations: classpath:com/richway/**/*Mapper.xml
  typeAliasesPackage: com.richway.**.entity
  global-config:
    id-type: 0
    field-strategy: 1
    db-column-underline: true
    logic-delete-value: 1
    logic-not-delete-value: 0
  configuration:
    map-underscore-to-camel-case: true
    cache-enabled: false
# swagger配置
swagger:
  enable: true
  title: 传真模块接口
  license: Powered By Richway
~~~
