* [1、项目架构](javascript:;)
    - [1.1、环境准备](item/env)
    - [1.2、项目搭建](item/start)
* [2、公共组件](javascript:;)
    - [2.1、common组件](common/common)
    - [2.2、gateway网关](common/gateway)
* [3、业务组件](javascript:;)
    - [3.1、传真组件](biz/fax)
    - [3.2、天气组件](biz/weather)
    - [3.3、云图组件](biz/cloud)
    - [3.4、文件预览组件](biz/file)
    - [3.5、全文检索组件](biz/es)

<div class="ew-doc-adv-list">
    
</div>
