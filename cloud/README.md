## 更新日志   :id=log

> 当前版本：`richway-cloud v1.0`，更新于：`2021-03-19`

### 2021-03-19 
- [新增] 天气组件
- [新增] 云图组件
- [新增] 文件预览组件
- [新增] 全文检索组件
- [完善] common组件，新增功能可开启服务只能被网关调用
- 更多组件开发中...
### 2021-03-17 （V1.0）   :id=log-317

- [新增] 网关`richway-cloud-gateway`搭建完成
- [新增] 公共组件`richway-cloud-common`搭建完成
- [新增] 传真组件`richway-cloud-fax`搭建完成

> 查看使用说明


