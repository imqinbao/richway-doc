### 1.1.1.IDEA插件配置 :id=idea

#### 1.1.1.1.Lombok 

项目使用`Lombok`来简化开发，[查看用法](https://projectlombok.org/)

### 1.1.2.MAVEN配置 :id=maven

maven私服仓库默认用的官方镜像，在下载依赖包的时候会比较慢，可以用阿里的私服仓库地址

~~~xml
<mirrors>
	  <mirror>  
      <id>alimaven</id>  
      <name>aliyun maven</name>  
  　　<url>http://maven.aliyun.com/nexus/content/groups/public/</url>  
      <mirrorOf>central</mirrorOf>          
    </mirror>  
</mirrors>
~~~



### 1.1.3.MYSQL安装配置 :id=mysql

MYSQL版本5.7，公司的云环境地址：

| 服务  | 地址               | 账号密码      |
| ----- | ------------------ | ------------- |
| mysql | 192.168.99.97:3306 | root   123456 |

### 1.1.4.REDIS安装配置 :id=redis

| 服务  | 地址               | 账号密码 |
| ----- | ------------------ | -------- |
| redis | 192.168.99.97:6379 | 空       |

redis桌面工具可以用`RedisDesktopManager` [下载地址](http://www.downza.cn/soft/210734.html)

### 1.1.5.NACOS安装配置 :id=nacos

nacos官网地址：[https://nacos.io/zh-cn/](https://nacos.io/zh-cn/)

配置`Nacos`持久化，修改`conf/application.properties`文件，增加支持`MySql`数据源的支持

~~~
# db mysql
spring.datasource.platform=mysql
db.num=1
db.url.0=jdbc:mysql://192.168.99.97:3306/richway-cloud-config?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true&useUnicode=true&useSSL=false&serverTimezone=UTC
db.user=root
db.password=123456
~~~

>配置文件application.properties是在下载的nacos-server包conf目录下。
>默认配置单机模式，nacos也集群/多集群部署模式参考 ([Nacos支持三种部署模式](https://nacos.io/zh-cn/docs/deployment.html)）

单机模式启动：

startup.cmd -m standalone

如果出现了错误：Caused by: java.net.UnknownHostException: jmenv.tbsite.net

说明启动模式没有设置。



### 1.1.6.SENTINEL安装配置 :id=sentinel

默认不需要安装Sentinel，针对流控和降级熔断的配置可以在nacos中配置，sentinel官网：

[https://sentinelguard.io/zh-cn/](https://sentinelguard.io/zh-cn/)

