## 1.1.开发环境 :id=info

用docker搭建的

| 服务  | 地址                     | 账号密码      |
| ----- | ------------------------ | ------------- |
| mysql | 192.168.99.97:3306       | root   123456 |
| redis | 192.168.99.97:6379       | 空            |
| nacos | 192.168.99.97:8848/nacos | nacos/nacos   |



~~~
docker pull nacos/nacos-server

mkdir -p /home/nacos/logs/
mkdir -p /home/nacos/init.d/

docker run -d
-e PREFER_HOST_MODE=ip
-e MODE=standalone
-e SPRING_DATASOURCE_PLATFORM=mysql
-e MYSQL_SERVICE_HOST=172.17.0.2
-e MYSQL_SERVICE_PORT=3306
-e MYSQL_SERVICE_USER=root
-e MYSQL_SERVICE_PASSWORD=123456
-e MYSQL_SERVICE_DB_NAME=richway-cloud-config
-e TIME_ZONE='Asia/Shanghai'
-v /home/nacos/logs:/home/nacos/logs
-p 8848:8848
--name nacos
--restart=always
nacos/nacos-server
~~~

## 1.2.项目依赖

### 1.2.1.微服务依赖

版本统一，都用最新的稳定版，后续其他组件服务也尽量用最新的稳定版本来开发。

| Spring Cloud Version    | Spring Cloud Alibaba Version | Spring Boot Version |
| ----------------------- | ---------------------------- | ------------------- |
| Spring Cloud Hoxton.SR8 | 2.2.5.RELEASE                | 2.3.2.RELEASE       |

| Spring Cloud Alibaba Version | Sentinel Version | Nacos Version | RocketMQ Version | Dubbo Version | Seata Version |
| ---------------------------- | ---------------- | ------------- | ---------------- | ------------- | ------------- |
| 2.2.5.RELEASE                | 1.8.0            | 1.4.1         | 4.4.0            | 2.7.8         | 1.3.0         |

POM 依赖引入

~~~xml
<spring-boot.version>2.3.2.RELEASE</spring-boot.version>
<spring-cloud.version>Hoxton.SR8</spring-cloud.version>
<spring-cloud-alibaba.version>2.2.5.RELEASE</spring-cloud-alibaba.version>

<!-- SpringCloud Alibaba 微服务 -->
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-alibaba-dependencies</artifactId>
    <version>2.2.5.RELEASE</version>
    <type>pom</type>
    <scope>import</scope>
</dependency>
<!-- SpringCloud 微服务 -->
 <dependency>
     <groupId>org.springframework.cloud</groupId>
     <artifactId>spring-cloud-dependencies</artifactId>
     <version>${spring-cloud.version}</version>
     <type>pom</type>
     <scope>import</scope>
</dependency>
<!-- SpringBoot -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-dependencies</artifactId>
    <version>2.3.2.RELEASE</version>
    <type>pom</type>
    <scope>import</scope>
</dependency>
~~~

### 1.2.2.工具依赖

~~~xml
<hutool.version>5.0.0</hutool.version>
<lombok.version>1.18.10</lombok.version>

<!-- hutool工具类 -->
<dependency>
    <groupId>cn.hutool</groupId>
    <artifactId>hutool-all</artifactId>
    <version>${hutool.version}</version>
</dependency>
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <version>${lombok.version}</version>
    <!--<optional>true</optional>-->
</dependency>
~~~



## 1.3.服务注册

服务注册地址：`192.168.99.97:8848`

组件需要注册到Nacos服务中心需要做的是：

1，引入依赖

~~~xml
<!-- SpringCloud Ailibaba Nacos -->
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
</dependency>

<!-- SpringCloud Ailibaba Nacos Config -->
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
</dependency>
~~~

2，新建配置文件`bootstrap.yml`

springboot配置中所有配置文件都会加载，优先级高的会覆盖优先级低的，形成互补配置

~~~yml
#nacos地址
richway:
  nacos:
    url: 127.0.0.1:8848
#端口
server:
  port: 8083
#日志配置
logging:
  level:
    ROOT: WARN
    cn.javabb: ERROR
    com.baomidou.mybatisplus: ERROR
# Spring
spring:
  application:
    # 应用名称
    name: richway-cloud-gen
  profiles:
    # 环境配置
    active: dev
  cloud:
    nacos:
      discovery:
        # 服务注册地址
        server-addr: ${richway.nacos.url}
      config:
        # 配置中心地址
        server-addr: ${richway.nacos.url}
        # 配置文件格式
        file-extension: yml
        # 共享配置
        shared-dataids: application-${spring.profiles.active}.${spring.cloud.nacos.config.file-extension}
~~~

3，在启动类上加入服务发现注解`@EnableDiscoveryClient`

示例：

~~~
@EnableDiscoveryClient
@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
public class GenApplication {

    public static void main(String[] args) {
        SpringApplication.run(GenApplication.class, args);
    }

}

~~~

在启动类的时候就可以在nacos服务列表看到注册的服务了

![image-20210303181620432](https://i.loli.net/2021/03/03/rMNEpPbqSzQyLZl.png)

4，在网关配置路由

~~~yaml
      routes:
       # 代码生成
        - id: javaitem-gen
          uri: lb://javaitem-gen	#服务名
          predicates:
            - Path=/gen/**		#映射路径
          filters:
            - StripPrefix=1
~~~



## 1.4.服务调用

服务调用采用`openfegin`

1,引入依赖：

~~~xml
<!-- SpringCloud Openfeign -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-openfeign</artifactId>
</dependency>
~~~

2,添加注解 

在组件中开启fegin功能，只需引入`@EnableRichwayFegin`，例如:

~~~java
@EnableRichwaySwagger
@EnableRichwayFeign
@MapperScan("com.richway.**.mapper")
@SpringCloudApplication
public class RichwayFaxApplication {
    public static void main(String[] args) {
        SpringApplication.run(RichwayFaxApplication.class, args);
        System.out.println("=====  Richway-Fax传真模块启动成功  =====");
    }
}

~~~

3，新建fegin调用类

~~~java
@FeignClient(contextId = "GenApiService",value = "javaitem-gen",fallbackFactory = GenApiFallback.class)
public interface GenApiService {
    /**
     * 获取模板列表
     * @return
     */
    @GetMapping("/api/generator/templates")
    Map<String, Object> listTpl();
}

~~~

@FeignClient:标明是个fegin调用类

value：需要调用的服务名

fallbackFactory：服务熔断异常配置

4，服务调用异常配置
