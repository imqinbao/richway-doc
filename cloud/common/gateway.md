`richway-cloud-gateway`公司网关是系统对外的唯一入口。在调用各个组件的api功能的时候不需要关心组件的host和端口是什么，这里都统一由gateway管理起来了，目前`richway-cloud-gateway`提供的功能包括token校验，限流，验证码校验等功能。

### 2.2.1.路由配置

#### 2.2.1.1.websocket配置方式

~~~yaml
spring:
  cloud:
    gateway:
      routes:
        - id: richway-sys-api
          uri: ws://localhost:9090/
          predicates:
            - Path=/sys/**
~~~



#### 2.2.1.2.http地址配置方式

~~~yaml
spring:
  cloud:
    gateway:
      routes:
        - id: richway-sys-api
          uri: http://localhost:9090/
          predicates:
            - Path=/sys/**
~~~

#### 2.2.1.3.注册中心配置方式(推荐)

~~~yaml
spring:
  cloud:
    gateway:
      routes:
        - id: richway-sys-api
          uri: lb://richway-sys-api
          predicates:
            - Path=/sys/**
~~~

### 2.2.2.限流配置

限流主要是控制系统的QPS，从而达到保护系统的目的。

sentinel支持通过在代码中定义的方式和通过本地文件加载规则配置的方式，也支持从nacos中读取配置的方式，系统中主要是采用nacos+sentinel实现的动态流控规则。

1，引入依赖

~~~xml
<!-- springcloud ailibaba nacos config -->
<dependency>
	<groupId>com.alibaba.cloud</groupId>
	<artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
</dependency>

<!-- sentinel datasource nacos -->
<dependency>
	<groupId>com.alibaba.csp</groupId>
	<artifactId>sentinel-datasource-nacos</artifactId>
</dependency>
~~~

2，开启nacos和sentinel功能，添加sentinel配置。

~~~yaml
spring:
  cloud:
    nacos:
      discovery:
        # 服务注册地址
        server-addr: ${richway.nacos.url}
      config:
        # 配置中心地址
        server-addr: ${richway.nacos.url}
        # 配置文件格式
        file-extension: yml
        # 共享配置
        shared-dataids: application-${spring.profiles.active}.${spring.cloud.nacos.config.file-extension}
    sentinel:
      # 取消控制台懒加载
      eager: true
      transport:
        # 控制台地址
        dashboard: 127.0.0.1:8718
      # nacos配置持久化
      datasource:
        ds1: #流控配置
          nacos:
            server-addr: ${richway.nacos.url}
            dataId: sentinel-gateway-flow
            groupId: DEFAULT_GROUP
            data-type: json
            rule-type: flow
        ds2: #降级配置
          nacos:
              server-addr: ${richway.nacos.url}
              dataId: sentinel-gateway-degrade
              groupId: DEFAULT_GROUP
              data-type: json
              rule-type: degrade
~~~

`sentinel-gateway-flow`对应`com.alibaba.csp.sentinel.slots.block.flow.FlowRule`各个属性

3，修改nacos配置中心的sentinel-gateway-flow

![image-20210325164520159](https://i.loli.net/2021/03/25/vSYKGuH6syE8ex2.png)

~~~json
[
	{
        "resource": "richway-gen",
        "count": 200,
        "grade": 1,
        "limitApp": "default",
        "strategy": 0,
        "controlBehavior": 0
    }
]
~~~

4，开启`sentinel`应用，可以看到我们在nacos中配置的规则

启动sentinel命令

~~~
java -Dserver.port=8718 -Dcsp.sentinel.dashboard.server=localhost:8718 -Dproject.name=sentinel-dashboard -jar D:\instal\sentinel\sentinel-dashboard-1.8.0.jar
~~~

可以看到在nacos配置中配置的流控规则

![image-20210325171631404](https://i.loli.net/2021/03/25/BUoVACwezMQDdti.png)

5，gateway集成sentinel

自定义限流异常处理器

~~~java
/**
 * 自定义限流异常处理
 */
public class SentinelFallbackHandler implements WebExceptionHandler {
    private Mono<Void> writeResponse(ServerResponse response, ServerWebExchange exchange) {
        ServerHttpResponse serverHttpResponse = exchange.getResponse();
        serverHttpResponse.getHeaders().add("Content-Type", "application/json;charset=UTF-8");
        byte[] datas = "{\"status\":429,\"message\":\"请求超过最大数，请稍后再试\"}".getBytes(StandardCharsets.UTF_8);
        DataBuffer buffer = serverHttpResponse.bufferFactory().wrap(datas);
        return serverHttpResponse.writeWith(Mono.just(buffer));
    }

    @Override
    public Mono<Void> handle(ServerWebExchange exchange, Throwable ex) {
        if (exchange.getResponse().isCommitted()) {
            return Mono.error(ex);
        }
        if (!BlockException.isBlockException(ex)) {
            return Mono.error(ex);
        }
        return handleBlockedRequest(exchange, ex).flatMap(response -> writeResponse(response, exchange));
    }

    private Mono<ServerResponse> handleBlockedRequest(ServerWebExchange exchange, Throwable throwable) {
        return GatewayCallbackManager.getBlockHandler().handleRequest(exchange, throwable);
    }

}
~~~

gateway集成sentinel

~~~java
@Configuration
public class GatewayConfig {

    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public SentinelFallbackHandler sentinelGatewayExceptionHandler() {
        return new SentinelFallbackHandler();
    }

    @Bean
    @Order(-1)
    public GlobalFilter sentinelGatewayFilter() {
        return new SentinelGatewayFilter();
    }
}

~~~

###  2.2.3.熔断降级

1，添加依赖：

~~~xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
</dependency>
~~~

2，配置需要熔断降级的服务

~~~yaml
      routes:
       # 代码生成
        - id: javaitem-gen
          uri: lb://javaitem-gen
          predicates:
            - Path=/gen/**
          filters:
            - StripPrefix=1
            - name: Hystrix
              args:
                name: default
                # 降级接口的地址
                fallbackUri: 'forward:/fallback'
~~~

> 上面包含了一个`Hystrix`过滤器，该过滤器会应用`Hystrix`的熔断与降级,会将请求包装成名为`fallback`的路由指令`RouteHystrixCommand`，`RouteHystrixCommand`继承于`HystrixObservableCommand`，其内包含了`Hystrix`的断路、资源隔离、降级等诸多断路器核心功能，当网关转发的请求出现问题时，网关能对其进行快速失败，执行特定的失败逻辑，保护网关安全。
>
> 配置中有一个可选参数`fallbackUri`，当前只支持`forward`模式的`URI`。如果服务被降级，请求会被转发到该`URI`对应的控制器。控制器可以是自定义的`fallback`接口；也可以使自定义的`Handler`，需要实现接口`org.springframework.web.reactive.function.server.HandlerFunction<T extends ServerResponse>`。

3，实现添加熔断降级的处理信息

~~~java
@Slf4j
@Component
public class HystrixFallbackHandler implements HandlerFunction<ServerResponse> {
    @Override
    public Mono<ServerResponse> handle(ServerRequest serverRequest) {
        Optional<Object> originalUris = serverRequest.attribute(ServerWebExchangeUtils.GATEWAY_ORIGINAL_REQUEST_URL_ATTR);
        originalUris.ifPresent(originalUri -> log.error("网关执行请求:{}失败,hystrix服务降级处理", originalUri));
        return ServerResponse.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(JSON.toJSONString(R.fail("服务已被降级熔断"))));
    }
}

~~~

4、路由配置信息加一个控制器方法用于处理重定向的`/fallback`请求

~~~java
@Configuration
public class RouterFunctionConfiguration {
    @Autowired
    private HystrixFallbackHandler hystrixFallbackHandler;

    @Autowired
    private ValidateCodeHandler validateCodeHandler;

    @SuppressWarnings("rawtypes")
    @Bean
    public RouterFunction routerFunction() {
        return RouterFunctions
                .route(RequestPredicates.path("/fallback").and(RequestPredicates.accept(MediaType.TEXT_PLAIN)),
                        hystrixFallbackHandler);
    }
}
~~~

### 2.2.4.白名单

配置的白名单不需要授权就可以直接访问

在`nacos`中配置`richway-gateway-dev.yml`

~~~yaml
# 不校验白名单
ignore:
  whites:
    - /auth/logout
    - /auth/login
    - /*/v2/api-docs
    - /csrf
    - /gen/**
    - /cloud/**
    # - /fax/**
~~~

### 2.2.5.全局过滤器

全局过滤器作用于所有的路由，不需要单独配置，我们可以用它来实现很多统一化处理的业务需求，比如权限认证，IP访问限制等等。目前网关统一鉴权AuthFilter.java就是采用的全局过滤器。

单独定义只需要实现GlobalFilter, Ordered这两个接口就可以了。

~~~java
@Slf4j
@Component
public class AuthFilter implements GlobalFilter, Ordered {
    // 排除过滤的 uri 地址，nacos自行添加
    @Autowired
    private IgnoreWhiteProperties ignoreWhite;
    @Resource(name = "stringRedisTemplate")
    private ValueOperations<String, String> sops;
    @Autowired
    private RedisService redisService;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        String url = exchange.getRequest().getURI().getPath();
        // 白名单直接通行
        if (WebUtils.matches(url, ignoreWhite.getWhites())) {
            return chain.filter(exchange);
        }
        String token = getToken(exchange.getRequest());
        if (StrUtil.isBlank(token)) {
            return setUnauthorizedResponse(exchange, "令牌不能为空");
        }
        String userStr = sops.get(getTokenKey(token));
        if (StrUtil.isEmpty(userStr)) {
            return setUnauthorizedResponse(exchange, "登录状态已过期");
        }
        JSONObject obj = JSONObject.parseObject(userStr);
        String userid = obj.getString("userid");
        String username = obj.getString("username");
        if (StrUtil.isBlank(userid) || StrUtil.isBlank(username)) {
            return setUnauthorizedResponse(exchange, "令牌验证失败");
        }

        // 设置过期时间
        redisService.expire(getTokenKey(token), ConsVal.TOKEN_EXPIRE_TIME);
        // 设置用户信息到请求
        /*ServerHttpRequest mutableReq = exchange.getRequest().mutate().header(ConsVal.DETAILS_USER_ID, userid)
                .header(ConsVal.DETAILS_USERNAME, username).build();
        ServerWebExchange mutableExchange = exchange.mutate().request(mutableReq).build();
        */
        /*新方法*/
        Consumer<HttpHeaders> httpHeaders = httpHeader->{
            httpHeader.set(ConsVal.DETAILS_USER_ID, userid);
            httpHeader.set(ConsVal.DETAILS_USERNAME, username);
            httpHeader.set(ConsVal.REQUEST_ORIGIN,ConsVal.REQUEST_ORIGIN_GATEWAY);
        };
        ServerHttpRequest serverHttpRequest = exchange.getRequest().mutate().headers(httpHeaders).build();
        ServerWebExchange mutableExchange = exchange.mutate().request(serverHttpRequest).build();
        return chain.filter(mutableExchange);
    }

    private Mono<Void> setUnauthorizedResponse(ServerWebExchange exchange, String msg) {
        ServerHttpResponse response = exchange.getResponse();
        response.getHeaders().setContentType(MediaType.APPLICATION_JSON);
        response.setStatusCode(HttpStatus.OK);

        log.error("[鉴权/异常处理]请求路径:{}", exchange.getRequest().getPath());
        return response.writeWith(Mono.fromSupplier(() -> {
            DataBufferFactory bufferFactory = response.bufferFactory();
            return bufferFactory.wrap(JSON.toJSONBytes(R.fail(HttpStatus.UNAUTHORIZED.value(),msg)));
        }));
    }

    private String getTokenKey(String token) {
        return ConsVal.LOGIN_TOKEN_KEY + token;
    }

    /**
     * 获取请求token
     */
    private String getToken(ServerHttpRequest request) {
        String token = request.getHeaders().getFirst(ConsVal.HEADER);
        if (StrUtil.isNotEmpty(token) && token.startsWith(ConsVal.TOKEN_PREFIX)) {
            token = token.replace(ConsVal.TOKEN_PREFIX, "");
        }
        return token;
    }

    @Override
    public int getOrder() {
        return -200;
    }
}

~~~
