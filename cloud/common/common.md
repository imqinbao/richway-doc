### 2.1.1.常用核心类

#### 2.1.1.1.常量类ConsVal

类的位置：`com.richway.common.constant`，里面包括系统中常用的一些常量

~~~java
/* 返回结果统一 */
    public static final int RESULT_OK_CODE = 0;  // 默认成功码
    public static final int RESULT_ERROR_CODE = 1;  // 默认失败码
    /**
     * 成功标记
     */
    public static final Integer RETURN_SUCCESS = 200;
    /**
     * 失败标记
     */
    public static final Integer RETURN_FAIL = 500;


    /* token 设置 */
    public static final Long TOKEN_EXPIRE_TIME = 60 * 60 * 24L;  // token过期时间,单位秒
    public static final int TOKEN_WILL_EXPIRE = 30;  // token将要过期自动刷新,单位分钟
    public static final String TOKEN_KEY = "ULgNsWJ8rPjRtnjzX/Gv2RGS80Ksnm/ZaLpvIL+NrBg=";  // 生成token的key
    /**
     * 请求来源
     */
    public static final String REQUEST_ORIGIN = "origin";
    /**
     * 来源 网关
     */
    public static final String REQUEST_ORIGIN_GATEWAY = "gateway";
    /**
     * 用户ID字段
     */
    public static final String DETAILS_USER_ID = "user_id";

    /**
     * 用户名字段
     */
    public static final String DETAILS_USERNAME = "username";
    /**
     * 令牌自定义标识
     */
    public static final String HEADER = "Authorization";

    /**
     * 令牌前缀
     */
    public static final String TOKEN_PREFIX = "Bearer ";

    /**
     * 权限缓存前缀
     */
    public final static String LOGIN_TOKEN_KEY = "login_tokens:";
    /**
     * 授权信息字段
     */
    public static final String AUTHORIZATION_HEADER = "authorization";

    /**
     * 验证码 redis key
     */
    public static final String CAPTCHA_CODE_KEY = "captcha_codes:";

    /**
     * 验证码有效期（分钟）
     */
    public static final long CAPTCHA_EXPIRATION = 2;
~~~

#### 2.1.1.2.返回类AjaxResult

~~~java
package com.richway.common.web.domain;

/**
 * 接口返回结果对象
 */
public class AjaxResult extends HashMap<String, Object> {

    private static final long serialVersionUID = 1L;

    private static final String CODE_NAME = "code";  // 状态码字段名称
    private static final String MSG_NAME = "msg";  // 状态信息字段名称
    private static final String DATA_NAME = "data";  // 数据字段名称
    private static final int DEFAULT_OK_CODE = ConsVal.RESULT_OK_CODE;  // 默认成功码
    private static final int DEFAULT_ERROR_CODE = ConsVal.RESULT_ERROR_CODE;  // 默认失败码
    private static final String DEFAULT_OK_MSG = "操作成功";  // 默认成功msg
    private static final String DEFAULT_ERROR_MSG = "操作失败";  // 默认失败msg

    private AjaxResult() {
    }

    /**
     * 返回成功
     */
    public static AjaxResult ok() {
        return ok(null);
    }

    /**
     * 返回成功
     */
    public static AjaxResult ok(String message) {
        return ok(DEFAULT_OK_CODE, message);
    }

    /**
     * 返回成功
     */
    public static AjaxResult ok(int code, String message) {
        if (message == null) message = DEFAULT_OK_MSG;
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.put(CODE_NAME, code);
        ajaxResult.put(MSG_NAME, message);
        return ajaxResult;
    }

    /**
     * 返回失败
     */
    public static AjaxResult error() {
        return error(null);
    }

    /**
     * 返回失败
     */
    public static AjaxResult error(String message) {
        return error(DEFAULT_ERROR_CODE, message);
    }

    /**
     * 返回失败
     */
    public static AjaxResult error(int code, String message) {
        if (message == null) message = DEFAULT_ERROR_MSG;
        return ok(code, message);
    }

    public AjaxResult setCode(Integer code) {
        return put(CODE_NAME, code);
    }

    public AjaxResult setMsg(String message) {
        return put(MSG_NAME, message);
    }

    public AjaxResult setData(Object object) {
        return put(DATA_NAME, object);
    }

    public Integer getCode() {
        Object o = get(CODE_NAME);
        return o == null ? null : Integer.parseInt(o.toString());
    }

    public String getMsg() {
        Object o = get(MSG_NAME);
        return o == null ? null : o.toString();
    }

    public Object getData() {
        return get(DATA_NAME);
    }

    @Override
    public AjaxResult put(String key, Object object) {
        if (key != null && object != null) super.put(key, object);
        return this;
    }

}
~~~



### 2.1.2.配置Swagger功能

在启动类上通过`@EnableRichwaySwagger`开启swagger功能

### 2.1.3.配置Fegin功能

在启动类上通过注解`@EnableRichwayFeign` 开启Fegin功能

参数支持一下几种用法：

~~~java
String[] value() default {};
String[] basePackages() default {"com.richway"};
Class<?>[] basePackageClasses() default {};
Class<?>[] defaultConfiguration() default {};
Class<?>[] clients() default {};
~~~

例如：

~~~
@EnableRichwaFeign(basePackages={"com.richway","cn.javabb"})
~~~



### 2.1.4.配置只能网关访问

在启动类上通过注解`@EnableRichwayAuth`开启功能

### 2.1.5.配置接口权限

在组件接口上通过`@PreAuthorize`，可以指定接口权限

参数支持以下几种用法：

~~~java
	/**
     * 验证用户是否具备某权限
     */
    public String hasPermi() default "";

    /**
     * 验证用户是否不具备某权限，与 hasPermi逻辑相反
     */
    public String lacksPermi() default "";

    /**
     * 验证用户是否具有以下任意一个权限
     */
    public String[] hasAnyPermi() default {};

    /**
     * 判断用户是否拥有某个角色
     */
    public String hasRole() default "";

    /**
     * 验证用户是否不具备某角色，与 isRole逻辑相反
     */
    public String lacksRole() default "";

    /**
     * 验证用户是否具有以下任意一个角色
     */
    public String[] hasAnyRoles() default {};

~~~

例如：

~~~
@PreAuthorize(hasPermi="sys:user:view")
~~~
